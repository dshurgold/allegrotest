- mountain 1/2 speed, iron production
- wood 1 speed, wood production
- water un-passable, trade production
- grass 2 speed, food production

- settlement can consume up to 1 space - no army production - settler production
- city can consume up to 2 spaces - food,  gold and wood to upgrade - one army production
- metropolis can consume up to 3 spaces - food, gold and wood to upgrade - two army production

- settler requires food and wood
- infantry requires iron and gold
- cavalry requires iron and gold

- infantry requires barracks - wood and gold
- cavalry required stables - wood and gold

------
construct (display)
resize(display)
draw()
------
