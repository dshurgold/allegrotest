/*
 * Player.h
 *
 *  Created on: Apr 15, 2014
 *      Author: David Shurgold
 */

#ifndef PLAYER_H_
#define PLAYER_H_

/**
 * Base player object that is shared across human and non-human alike
 */
class Player {
public:
    Player();
    virtual ~Player();
private:
    long wood;
    long gold;
    long iron;
};

#endif /* PLAYER_H_ */
