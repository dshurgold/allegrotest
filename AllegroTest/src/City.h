/*
 * City.h
 *
 *  Created on: Apr 15, 2014
 *      Author: david
 */

#ifndef CITY_H_
#define CITY_H_

#include "MapElement.h"

class City: protected MapElement {
public:
    City();
    virtual ~City();
private:
    long wood_production;
    long gold_production;
    long iron_production;

    std::array<MapPoint, 37> used_tiles;
};

#endif /* CITY_H_ */
