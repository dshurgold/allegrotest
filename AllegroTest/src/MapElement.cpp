/*
 * MapElement.cpp
 *
 *  Created on: Apr 15, 2014
 *      Author: david
 */

#include "MapElement.h"

MapElement::MapElement() {
    //this->point = bg::model::point<MapIndex, 2, bg::cs::cartesian>;
    // TODO Auto-generated constructor stub

}

MapElement::~MapElement() {
    // TODO Auto-generated destructor stub
}

MapElementType MapElement::getType() {
    return type;
}

MapIndex MapElement::getX() {
    return this->location.get<0>();
}

void MapElement::setX(MapIndex x) {
    this->location.set<0>(x);
}

MapIndex MapElement::getY() {
    return this->location.get<1>();
}

void MapElement::setY(MapIndex y) {
    this->location.set<1>(y);
}
