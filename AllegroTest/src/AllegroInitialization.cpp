/*
 * AllegroInitialization.cpp
 *
 *  Created on: Apr 6, 2014
 *      Author: david
 */

#include "AllegroInitialization.h"

AllegroInitialization::AllegroInitialization() {
    // TODO Auto-generated constructor stub

}

/**
 * Initialize the allegro keyboard, image, timer, display and event queue
 */
void AllegroInitialization::init(int frame_rate, int width, int height) {
    if (!al_init()) {
        throw std::runtime_error("Failed to initialize allegro");
    }
    if (!al_install_keyboard()) {
        throw std::runtime_error("Failed to initialize the keyboard");
    }
    if (!al_install_mouse()) {
        throw std::runtime_error("Failed to initialize the mouse");
    }
    if (!al_init_image_addon()) {
        throw std::runtime_error("Failed to initialize image addon");
    }

    al_init_font_addon();
    al_init_ttf_addon();

    this->timer = al_create_timer(1.0 / frame_rate);
    if (!this->timer) {
        throw std::runtime_error("Failed to create timer");
    }
    this->display = al_create_display(width, height);
    if (!this->display) {
        throw std::runtime_error("Failed to create display");
    }

    this->event_queue = al_create_event_queue();
    if (!this->event_queue) {
        throw std::runtime_error("Failed to create event queue");
    }

    al_register_event_source(this->event_queue, al_get_display_event_source(this->display));
    al_register_event_source(this->event_queue, al_get_timer_event_source(this->timer));
    al_register_event_source(this->event_queue, al_get_keyboard_event_source());
    al_start_timer(this->timer);
}

ALLEGRO_DISPLAY *AllegroInitialization::getDisplay() {
    return this->display;
}

ALLEGRO_EVENT_QUEUE *AllegroInitialization::getEventQueue() {
    return this->event_queue;
}

ALLEGRO_TIMER *AllegroInitialization::getTimer() {
    return this->timer;
}

ALLEGRO_MOUSE_STATE *AllegroInitialization::getMouseState() {
    al_get_mouse_state(&this->mouse_state);
    return &this->mouse_state;
}

AllegroInitialization::~AllegroInitialization() {
    al_destroy_display(this->display);
    al_destroy_event_queue(this->event_queue);
    al_destroy_timer(this->timer);
}
