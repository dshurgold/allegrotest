/*
 * AllegroInitialization.h
 *
 *  Created on: Apr 6, 2014
 *      Author: david
 */

#ifndef ALLEGROINITIALIZATION_H_
#define ALLEGROINITIALIZATION_H_

#include <stdexcept>
#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>

/**
 * Support all the initialization and screen creation routines required by allegro
 */
class AllegroInitialization {
public:
    AllegroInitialization();
    void init(int frame_rate, int width, int height);
    virtual ~AllegroInitialization();
    ALLEGRO_DISPLAY *getDisplay();
    ALLEGRO_EVENT_QUEUE *getEventQueue();
    ALLEGRO_TIMER *getTimer();
    ALLEGRO_MOUSE_STATE *getMouseState();

private:
    ALLEGRO_DISPLAY *display;
    ALLEGRO_EVENT_QUEUE *event_queue;
    ALLEGRO_TIMER *timer;
    ALLEGRO_MOUSE_STATE mouse_state;
};

#endif /* ALLEGROINITIALIZATION_H_ */
