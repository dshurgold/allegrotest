/*
 * main.cpp
 *
 *  Created on: Mar 30, 2014
 *      Author: David Shurgold
 */

#include "config.h"
#include <iostream>
#include "AllegroInitialization.h"
#include "MapGenerator.h"
#include "StatusBarDisplay.h"
#include "MapManager.h"

int main(int argc, char **argv) {
    AllegroInitialization *allegro = NULL;
    MapGenerator *map_gen = NULL;
    MapManager *map_manager = NULL;
    StatusBarDisplay *bar = NULL;
    std::array<bool, ALLEGRO_KEY_MAX> pressed_keys;
    int view_left = 0, view_top = 0;
    bool redraw = false;
    for(int i = 0;i<ALLEGRO_KEY_MAX;i++) {
        pressed_keys[i] = false;
    }

    allegro = new AllegroInitialization();
    allegro->init(FPS, SCREEN_W, SCREEN_H);

    map_gen = new MapGenerator();
    map_gen->setSize(25, 25);
    MapArray map = map_gen->generate();
    map_manager = new MapManager(&map, allegro->getDisplay());
    bar = new StatusBarDisplay(allegro->getDisplay());

    while (1) {
        ALLEGRO_EVENT ev;
        al_wait_for_event(allegro->getEventQueue(), &ev);
        ALLEGRO_MOUSE_STATE *mouse_state = allegro->getMouseState();
        if (ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE) {
            break;
        } else if (ev.type == ALLEGRO_EVENT_TIMER) {
            if(pressed_keys[ALLEGRO_KEY_UP]) {
                view_top -= 5;
                if(view_top < -SCREEN_H) {
                    view_top = -SCREEN_H;
                }
            }
            if(pressed_keys[ALLEGRO_KEY_DOWN]) {
                view_top += 5;
                if(view_top > SCREEN_H) {
                    view_top = SCREEN_H;
                }
            }
            if(pressed_keys[ALLEGRO_KEY_LEFT]) {
                view_left -= 5;
                if(view_left < -SCREEN_W) {
                    view_left = -SCREEN_W;
                }
            }
            if(pressed_keys[ALLEGRO_KEY_RIGHT]) {
                view_left += 5;
                if(view_left > SCREEN_W) {
                    view_left = SCREEN_W;
                }
            }
            if(pressed_keys[ALLEGRO_KEY_C]) {
                MapPoint point = map_manager->getMapPosition(mouse_state->x, mouse_state->y);
            }
            redraw = true;
        } else if(ev.type == ALLEGRO_EVENT_KEY_DOWN) {
            pressed_keys[ev.keyboard.keycode] = true;
        } else if(ev.type == ALLEGRO_EVENT_KEY_UP) {
            pressed_keys[ev.keyboard.keycode] = false;
            if(ev.keyboard.keycode == ALLEGRO_KEY_ESCAPE) {
                break;
            }
        }

        if (redraw && al_is_event_queue_empty(allegro->getEventQueue())) {
            redraw = false;
            al_clear_to_color(al_map_rgb(0, 0, 0));
            map_manager->draw(view_left, view_top, mouse_state->x, mouse_state->y);
            bar->draw();
            al_flip_display();
        }
    }

    delete allegro;
    return 0;
}
