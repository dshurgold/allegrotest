/*
 * StatusBarDisplay.cpp
 *
 *  Created on: Apr 17, 2014
 *      Author: David Shurgold
 */

#include "StatusBarDisplay.h"

StatusBarDisplay::StatusBarDisplay(ALLEGRO_DISPLAY *display) {
    font = al_load_ttf_font("resources/DejaVuSans.ttf", 18, 0);
    if (!font) {
        throw std::runtime_error("Failed to load resources/DejaVuSans.ttf font");
    }
    //194 - 66 for title - 128 for status

    bar = al_create_bitmap(al_get_display_width(display), 28);
    wood_value = al_create_sub_bitmap(bar,  66, 0, 128, 28);
    iron_value = al_create_sub_bitmap(bar, 260, 0, 128, 28);
    gold_value = al_create_sub_bitmap(bar, 454, 0, 128, 28);

    al_set_target_bitmap(bar);
    al_clear_to_color(al_map_rgb(0, 0, 0));
    al_draw_text(font, al_map_rgb(255,255,255),   4, 3, ALLEGRO_ALIGN_LEFT, "Wood:");
    al_draw_text(font, al_map_rgb(255,255,255), 198, 3, ALLEGRO_ALIGN_LEFT, "Iron:");
    al_draw_text(font, al_map_rgb(255,255,255), 392, 3, ALLEGRO_ALIGN_LEFT, "Gold:");
    al_set_target_bitmap(al_get_backbuffer(display));
    this->display = display;
}

StatusBarDisplay::~StatusBarDisplay() {
    // TODO Auto-generated destructor stub
}

void StatusBarDisplay::draw() {
    al_set_target_bitmap(wood_value);
    al_clear_to_color(al_map_rgb(0, 0, 0));
    al_draw_text(font, al_map_rgb(255, 255, 255), 0, 3, ALLEGRO_ALIGN_LEFT, "10000 (+1000)");
    al_set_target_bitmap(iron_value);
    al_clear_to_color(al_map_rgb(0, 0, 0));
    al_draw_text(font, al_map_rgb(255, 255, 255), 0, 3, ALLEGRO_ALIGN_LEFT, "10000 (+1000)");
    al_set_target_bitmap(gold_value);
    al_clear_to_color(al_map_rgb(0, 0, 0));
    al_draw_text(font, al_map_rgb(255, 255, 255), 0, 3, ALLEGRO_ALIGN_LEFT, "10000 (+1000)");
    al_set_target_bitmap(al_get_backbuffer(display));
    al_draw_bitmap(bar, 0, 0, 0);
}
