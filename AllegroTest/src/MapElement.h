/*
 * MapElement.h
 *
 *  Created on: Apr 15, 2014
 *      Author: david
 */

#ifndef MAPELEMENT_H_
#define MAPELEMENT_H_

#include "Map.h"

/**
 * Base map element that is used for any element that could be potentially displayed
 */
class MapElement {
public:
    MapElement();
    virtual ~MapElement();

    MapElementType getType();
    MapIndex getX();
    void setX(MapIndex x);
    MapIndex getY();
    void setY(MapIndex y);

private:
    MapPoint location;
protected:
    MapElementType type;
};

#endif /* MAPELEMENT_H_ */
