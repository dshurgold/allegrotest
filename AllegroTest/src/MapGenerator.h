/*
 * MapGenerator.h
 *
 *  Created on: Apr 5, 2014
 *      Author: David Shurgold
 */

#ifndef MAPGENERATOR_H_
#define MAPGENERATOR_H_

#include "Map.h"
#include <boost/random/random_device.hpp>
#include <boost/random/discrete_distribution.hpp>

/**
 * Generate maps based on a very simple random with weighting based on the mixes provided
 */
class MapGenerator {
public:
    MapGenerator();
    void setSize(int width, int height);
    bool setMix(int forest, int water, int mountain, int grass);
    MapArray generate();
    virtual ~MapGenerator();
private:
    int width, height;
    double forest, water, mountain, grass;
};

#endif /* MAPGENERATOR_H_ */
