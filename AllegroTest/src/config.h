/*
 * config.h
 *
 *  Created on: Apr 15, 2014
 *      Author: David Shurgold
 */

#ifndef CONFIG_H_
#define CONFIG_H_

const float FPS = 60;
const int SCREEN_W = 1024;//640;
const int SCREEN_H = 768;//480;


#endif /* CONFIG_H_ */
