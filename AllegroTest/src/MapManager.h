/*
 * MapManager.h
 *
 *  Created on: Apr 5, 2014
 *      Author: David Shurgold
 */

#ifndef MAPMANAGER_H_
#define MAPMANAGER_H_

#include <iostream>
#include <array>
#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include "Map.h"


class MapManager {
public:
    MapManager(MapArray*, ALLEGRO_DISPLAY*);
    void draw(int display_left, int display_top, int mouse_x, int mouse_y);
    long getTotalHeight();
    long getTotalWidth();
    long getEffectiveHeight();
    long getEffectiveWidth();
    MapPoint getMapPosition(int x, int y);
    void print();
    virtual ~MapManager();
private:
    const int width_effective = 43;
    const int height_effective = 50;
    const int height_offset = 25;
    std::array<ALLEGRO_BITMAP*, 4> bitmaps;
    ALLEGRO_BITMAP *cursor_mark;
    ALLEGRO_DISPLAY *display;
    MapArray *map;
};

#endif /* MAPMANAGER_H_ */
