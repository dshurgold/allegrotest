/*
 * StatusBarDisplay.h
 *
 *  Created on: Apr 17, 2014
 *      Author: David Shurgold
 */

#ifndef STATUSBARDISPLAY_H_
#define STATUSBARDISPLAY_H_

#include <stdexcept>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>

class StatusBarDisplay {
public:
    StatusBarDisplay(ALLEGRO_DISPLAY *display);
    virtual ~StatusBarDisplay();
    void draw();
private:
    ALLEGRO_DISPLAY *display;
    ALLEGRO_FONT *font;
    ALLEGRO_BITMAP *bar, *wood_value, *iron_value, *gold_value;
};

#endif /* STATUSBARDISPLAY_H_ */
