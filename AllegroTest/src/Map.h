/*
 * Map.h
 *
 *  Created on: Apr 5, 2014
 *      Author: David Shurgold
 */

#ifndef MAP_H_
#define MAP_H_

#include <boost/multi_array.hpp>
#include <boost/geometry.hpp>

/**
 * Constants and typedefs to support map management and creation
 */
typedef enum {
  FOREST = 0,
  WATER = 1,
  MOUNTAIN = 2,
  GRASS = 3
} MapTypes;

/**
 * Doesn't belong here - or need to change include
 */
typedef enum {
    SETTLEMENT,
    CITY,
    METRO,
    SETTLER,
    INFANTRY,
    CAVALRY,

} MapElementType;


typedef boost::multi_array<MapTypes, 2> MapArray;
typedef MapArray::index MapIndex;
typedef boost::geometry::model::point<MapIndex, 2, boost::geometry::cs::cartesian> MapPoint;

#endif /* MAP_H_ */
