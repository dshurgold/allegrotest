/*
 * MapManager.cpp
 *
 *  Created on: Apr 5, 2014
 *      Author: David Shurgold
 */

#include "MapManager.h"

MapManager::MapManager(MapArray *map, ALLEGRO_DISPLAY *display) {
    this->map = map;
    this->display = display;
    this->bitmaps[FOREST] = al_load_bitmap("resources/forest.png");
    if (!this->bitmaps[FOREST]) {
        throw std::runtime_error("Failed to load forest.png");
    }
    this->bitmaps[WATER] = al_load_bitmap("resources/water.png");
    if (!this->bitmaps[WATER]) {
        throw std::runtime_error("Failed to load water.png");
    }
    this->bitmaps[MOUNTAIN] = al_load_bitmap("resources/mountain.png");
    if (!this->bitmaps[MOUNTAIN]) {
        throw std::runtime_error("Failed to load mountain.png");
    }
    this->bitmaps[GRASS] = al_load_bitmap("resources/grass.png");
    if (!this->bitmaps[GRASS]) {
        throw std::runtime_error("Failed to load grass.png");
    }
    this->cursor_mark = al_load_bitmap("resources/cursor.png");
    if (!this->cursor_mark) {
        throw std::runtime_error("Failed to load cursor.png");
    }
}

void MapManager::print() {
    std::cout << "PRINT" << std::endl;
    for (MapIndex w = 0; w < this->map->shape()[0]; w++) {
        for (MapIndex h = 0; h < this->map->shape()[1]; h++) {
            std::cout << (*this->map)[w][h] << " ";
        }
        std::cout << std::endl;
    }
}

long MapManager::getTotalHeight() {
    return (this->map->shape()[1] + 1) * height_effective - 25;
}

long MapManager::getTotalWidth() {
    return (this->map->shape()[0] + 1) * width_effective - 28;
}

long MapManager::getEffectiveHeight() {
    return height_effective;
}

long MapManager::getEffectiveWidth() {
    return width_effective;
}

void MapManager::draw(int display_left, int display_top, int mouse_x, int mouse_y) {
    long left, top, offset_left, offset_top;
    MapIndex start_left = floor(display_left / width_effective);
    offset_left = display_left - (start_left * width_effective);
    MapIndex start_top = floor(display_top / height_effective);
    offset_top = display_top - (start_top * height_effective);
    MapPoint cursor = this->getMapPosition(mouse_x + offset_left, mouse_y + offset_top);
    cursor.set<0>(cursor.get<0>() + start_left);
    cursor.set<1>(cursor.get<1>() + start_top);

    MapIndex width_max = ceil(al_get_display_width(this->display) / width_effective) + start_left + 1;
    if (width_max >= this->map->shape()[0]) {
        width_max = this->map->shape()[0] - 1;
    }
    MapIndex height_max = ceil(al_get_display_height(this->display) / height_effective) + start_top + 1;
    if (height_max >= this->map->shape()[1]) {
        height_max = this->map->shape()[1] - 1;
    }
    MapIndex map_left = start_left - 1;
    if (map_left < 0) {
        map_left = 0;
    }

    for (; map_left <= width_max; map_left++) {
        MapIndex map_top = start_top - 1;
        if (map_top < 0) {
            map_top = 0;
        }
        for (; map_top <= height_max; map_top++) {
            left = (map_left - start_left) * width_effective - offset_left;
            if (map_left % 2) {
                top = (map_top - start_top) * height_effective + height_offset - offset_top;
            } else {
                top = (map_top - start_top) * height_effective - offset_top;
            }
            al_draw_bitmap((ALLEGRO_BITMAP*) this->bitmaps[(*this->map)[map_left][map_top]], left, top, 0);
            if(map_left == cursor.get<0>() && map_top == cursor.get<1>()) {
                al_draw_bitmap(cursor_mark, left, top, 0);
            }
        }
    }
}

MapPoint MapManager::getMapPosition(int x, int y) {
    MapPoint location;
    location.set<0>(floor(x / width_effective));
    if (location.get<0>() % 2) {
        location.set<1>(floor((y - height_offset) / height_effective));
    } else {
        location.set<1>(floor(y / height_effective));
    }
    return location;
}

MapManager::~MapManager() {
    // TODO Auto-generated destructor stub
}

