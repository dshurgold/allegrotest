/*
 * MapGenerator.cpp
 *
 *  Created on: Apr 5, 2014
 *      Author: David Shurgold
 */

#include "MapGenerator.h"

MapGenerator::MapGenerator() {
    this->width = 200;
    this->height = 200;
    this->forest = 35;
    this->water = 15;
    this->mountain = 10;
    this->grass = 40;
}

void MapGenerator::setSize(int width, int height) {
    this->width = width;
    this->height = height;
}

/**
 * If mix doesn't equal 100 then the values are ignored and false is returned
 */
bool MapGenerator::setMix(int forest, int water, int mountain, int grass) {
    int total = forest + water + mountain + grass;
    if(total != 100) {
        return false;
    }
    this->forest = forest / 100;
    this->water = water / 100;
    this->mountain = mountain / 100;
    this->grass = grass / 100;
    return true;
}

/**
 * Simple random generation based on the values provided by the mix
 */
MapArray MapGenerator::generate() {
    MapArray map(boost::extents[this->width][this->height]);
    double probabilities[] = { this->forest, this->water, this->mountain, this->grass };
    boost::random::discrete_distribution<> map_type(probabilities);
    boost::random::random_device random_gen;

    for(MapIndex w = 0; w < this->width; w++) {
        for(MapIndex h = 0; h < this->width; h++) {
            map[w][h] = (MapTypes)(map_type(random_gen));
        }
    }
    return map;
}

MapGenerator::~MapGenerator() {
    // TODO Auto-generated destructor stub
}

